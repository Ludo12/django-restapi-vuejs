from django.contrib import admin
from .models import Task


class TaskAdmin(admin.ModelAdmin):
    fields = ('title', 'description')
    list_display = ('title', 'created_at', 'updated_at')
    # filter_horizontal = ('tags',)


admin.site.register(Task, TaskAdmin)
